package io.onebrick.sdk.ui.pinview

import android.content.res.Resources

class PinviewUIUtils {
    companion object {
        fun dpToPx(dp: Float): Float {
            return dp * Resources.getSystem().displayMetrics.density
        }
    }
}